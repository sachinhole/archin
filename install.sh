#!/bin/bash
printf '\033c' # clear screen
#====[ A R C H I N ]====================================================================================

# simple error message wrapper
err(){ echo >&2 "$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)"; exit 1337
}
# simple warning message wrapper
warn(){ echo >&2 "$(tput bold; tput setaf 1)[!] WARNING: ${*}$(tput sgr0)"
}
# simple echo wrapper
msg(){ echo "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"
}
# center text
CNT(){ echo "${1}" | sed  -e :a -e "s/^.\{1,$(tput cols)\}$/ & /;ta" | tr -d '\n' | head -c "$(tput cols)"
}
# simple title wrapper
TITLE(){ clear
    echo && CNT "[ ARCHIN  ]   "
    CNT "arch linux install script    "
    CNT "=========================================================================   "
    echo
}
# check root
CHECK_ROOT(){
    [[ "root" == "$(whoami)" ]] || err "cannot perform this operation unless run as root"
}
# check internet connection
CHECK_INTERNET(){
    ping -q -c1 google.com &>/dev/null || err "Please! connect to internet"
}
# source config file
CONFIG(){ msg "Importing config file"
    CFG_CONTENT=$( grep -v "^#" "$CFG_FILE" | grep "\S" | awk '{print $1"="$2}' )
    eval "$CFG_CONTENT"
}
#======================================================================================================

# mount options
optbtrfs='autodefrag,compress=lzo,discard=async,noatime,space_cache=v2,ssd,ssd_spread'
# base packages
PKG='base base-devel linux-firmware btrfs-progs vim git intel-ucode xf86-video-intel networkmanager'
# config file path
CFG_FILE=archin.conf
#---[ creat partition ]--------------------------------------------------------------------------------

CRPART(){
    echo -e " 1 > fdisk\n 2 > cfdisk\n 3 > none\n"
    read -p " Select tool to create partition:" TOOL
    case "$TOOL" in
        1) fdisk "$diskName"    ;;
        2) fdisk "$diskName"    ;;
        3) none                 ;;
        *) warn "Please! choose correct option"
    esac
}

#---[ partition formating ]----------------------------------------------------------------------------

# fat32 filesystem boot partition
EFI(){
    [[ "$formatBoot" == "Yes" ]] && mkfs.fat -F32 "$bootPart"  # format to fat32
    mount "$bootPart" /mnt/boot/efi                            # mount boot
}

# btrfs filesystem with subvolumes
BTSRF(){
    [[ "$formatRoot" == "No" ]] || mkfs.btrfs -f "$rootPart"   # formate to btrfs
    mount "$rootPart" /mnt || err "can not mount "        # mount root
    btrfs su cr /mnt/@                                       # create root subvol
    btrfs su cr /mnt/@home                                   # create home subvol
    btrfs su cr /mnt/@var                                    # create var subvol
    umount /mnt                                              # unmount root
    mount -o $optbtrfs,subvol=@ "$rootPart" /mnt                   # mount root subvol
    mkdir -p /mnt/{boot/efi,home,var}                        # creat directory
    mount -o $optbtrfs,subvol=@home "$rootPart" /mnt/home          # mount home subvol
    mount -o $optbtrfs,subvol=@var "rootPart" /mnt/var            # mount var subvol
}

# ext4 filesystem
EXT4(){
    [[ "$formatRoot" == "No" ]] || mkfs.ext4 "$rootPart"       # format to ext4
    mount "$rootPart" /mnt && mkdir -p /mnt/boot/efi           # mount root
}

#---[ install base arch linux ]------------------------------------------------------------------------

ARCH_BASE(){
    pacstrap /mnt "$PKG" "$linuxKernal" "${linuxKernal}"-headers   # install essential packages
    genfstab -U /mnt >> /mnt/etc/fstab                       # generate an fstab file
}
POSTSCRIPT(){
	awk '/\[ ARCH LINUX POST INSTALL \]/','/#\[ THE END \]/' "$0" > /mnt/postscript.sh
	chmod +x /mnt/postscript.sh
}
ARCH_CHROOT(){
	arch-chroot /mnt "$@"
}
CLEANUP(){
    rm -rf /mnt/postscript.sh
    read -p " [*] Press Enter To Reboot: " YN
    umount -a ; reboot
}

#====[ ARCH LINUX BASE INSTALL ]========================================================================

CHECK_INTERNET                                               # check internet connection
CHECK_ROOT                                                   # check root previlages

pacman --noconfirm -Syy archlinux-keyring                    # sync database and update archlinux-keyring
timedatectl set-ntp true                                     # set time

TITLE                                                        # HEADER
CONFIG                                                       # import config file

CRPART                                                       # create partitions
[[ "$fileSys" == "btrfs" ]] && BTRFS || EXT4                 # set filesystem type, format and mount
EFI                                                          # format and mount boot partition

ARCH_BASE                                                    # install arch linux base

#POSTSCRIPT                                                   # copy script to arch installation
#ARCH_CHROOT ./postscript.sh                                  # arch-chroot and run postinstall

CLEANUP                                                      # remove files, umount and reboot

#====[ ARCH LINUX POST INSTALL ]========================================================================
INSTALL_PKG(){
# arch linux standard repository
    pacman --needed --noconfirm -S $(awk '/[*]/ {print $2}' pkg)
# arch linux user repository
    baph -i -N $(awk '/[A]/ {print $2}' pkg)
# through git
    awk '/[G]/ {print $2 $3}' pkg | while read GPKG URL; do
        msg "Installing '$GPKG'"
        git clone "$URL" /opt/"$GPKG" && cd "$GPKG" && makepkg -si
        cd - && rm -rf "$GPKG"
    done
}
#====[ THE END ]========================================================================================
