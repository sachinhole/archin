
-------< x11 >------------------------------------------------------------------------------------------------------------------
[*] xorg-server                       # Xorg X server 
[*] xorg-xkill                        # Kill a client by its X resource 
[*] xorg-xsetroot                     # Classic X utility to set your root window background to a given pattern or color 
[*] xorg-xbacklight                   # RandR-based backlight control application 
[*] xorg-xprop                        # Property displayer for X 
[*] xorg-xwininfo                     # Command-line utility to print information about windows on an X server
[*] xclip                             # Command line interface to the X11 clipboard 

-------< window manager >-------------------------------------------------------------------------------------------------------
[*] bspwm                             # Tiling window manager based on binary space partitioning 
[*] sxhkd                             # Simple X hotkey daemon 

-------< grk themes >-----------------------------------------------------------------------------------------------------------
[*] papirus-icon-theme
[*] arc-gtk-theme

-------< audio >----------------------------------------------------------------------------------------------------------------
[*] pipewire                          # Low-latency audio/video router and processor 
[*] pipewire-pulse                    # Low-latency audio/video router and processor - PulseAudio replacement
[*] pipewire-jack                     # Low-latency audio/video router and processor - JACK support
[*] pipewire-alasa                    # Low-latency audio/video router and processor - ALSA configuration
[*] pulsemixer                        # CLI and curses mixer for pulseaudio

-------< fonts >---------------------------------------------------------------------------------------------------------------
[*] adobe-source-code-pro-fonts       # Monospaced font family for user interface and coding environments
[*] archcraft-fonts                   # Fonts for Archcraft
[*] cantarell-fonts                   # Humanist sans serif font
[*] fontconfig                        # Library for configuring and customizing font access
[*] libfontenc                        # X11 font encoding library
[*] libxfont2                         # X11 font rasterisation library
[*] noto-fonts                        # Google Noto TTF fonts
[*] noto-fonts-emoji                  # Google Noto emoji fonts
[*] terminus-font                     # Monospace bitmap font (for X11 and console)
[*] ttf-jetbrains-mono
[*] ttf-joypixels
[*] ttf-font-awesome
[*] xorg-fonts-alias-misc             # X.org font alias files - misc font familiy
[*] xorg-fonts-encodings              # X.org font encoding files
[*] xorg-fonts-misc                   # X.org misc fonts

------
[*] alacritty                         # A cross-platform, GPU-accelerated terminal emulator 
[*] rofi                              # A window switcher, application launcher and dmenu replacement 
[*] dunst                             # Customizable and lightweight notification-daemon 
[*] feh                               # Fast and light imlib2-based image viewer 
[*] fzf                               # Command-line fuzzy finder 
[*] sxiv                              # Simple X Image Viewer 
[*] mpv                               # a free, open source, and cross-platform media player 
[*] zathura                           # Minimalistic document viewer 
[*] zathura-pdf-mupdf                 # PDF support for Zathura (MuPDF backend) (Supports PDF, ePub, and OpenXPS) 
[*] ffmpeg                            # Complete solution to record, convert and stream audio and video 
[*] imagemagick                       # An image viewing/manipulation program 
[*] ranger                            # Simple, vim-like file manager 
[*] pcmanfm                           # Extremely fast and lightweight file manager 
[*] leafpad                           # A notepad clone for GTK+ 2.0
[*] notes-up                          # Write beautiful notes fast and easy using Markdown
[*] sticky-git                        # A sticky notes app for the Linux desktop 
[*] flameshot

-------
[*] xfce4-terminal                    # A modern terminal emulator primarily for the Xfce desktop environment 
[*] unrar                             # The RAR uncompression program 
[*] zip                               # Compressor/archiver for creating and modifying zipfiles 
[*] unzip                             # For extracting and viewing files in .zip archives 
[*] p7zip                             # Command-line file archiver with high compression ratio 
[*] git                               # the fast distributed version control system 
[*] curl                              # An URL retrieval utility and library 
[*] wget                              # Network utility to retrieve files from the Web 
[*] rsync                             # A fast and versatile file copying tool for remote and local files 
[*] dash                              # POSIX compliant shell that aims to be as small as possible 

-------< editors >--------------------------------------------------------------------------------------------------------------
[*] vim                               # Vi Improved, a highly configurable, improved version of the vi text editor 
[*] neovim                            # Fork of Vim aiming to improve user experience, plugins, and GUIs 
[*] aria2                             # Download utility that supports HTTP(S), FTP, BitTorrent, and Metalink 
[*] youtube-dl                        # A command-line program to download videos from YouTube.com and a few more sites 

-------< browsers >-------------------------------------------------------------------------------------------------------------
[*] firefox                           # Standalone web browser from mozilla.org 
[ ] vivaldi                           # An advanced browser made with the power user in mind.
[*] surf                              # A WebKit based browser
[ ] vimb                              # The vim like browser

-------< code >-----------------------------------------------------------------------------------------------------------------
[*] python                            # Next generation of the python high-level scripting language [ version:latest ]
[*] python2                           # A high-level scripting language [ version: 2 ]
[ ] gcc                               # The GNU Compiler Collection - C and C++ frontends
[ ] go                                

=======[ Arch user repository ]=================================================================================================

[A] timeshift                         # A system restore utility for Linux 
[A] ly
[A] python-pywal                      # Generate and change colorschemes on the fly 
[A] picom-ibhagwan-git                # iBhagwan's picom fork (X compositor) with dual_kawase blur and rounded corners

=======[ Git ]==================================================================================================================


