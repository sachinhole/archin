#!/usr/bin/bash
# Arch Linux packages
PKG='dialog openssh wireless_tools netctl htop dash vim neovim aria2c unrar zip unzip 
p7zip git curl wget rsync '
#------------------------------------------------------------------------------------
printf '\033c'      # clear screen
. ./archin.conf     # import achin config
#====================================================================================#
#                         P O S T * I N S T A L L A T I O N                          #
#====================================================================================#
# Set time zone
ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime

# Run hwclock to generate /etc/adjtime
hwclock --systohc

# Edit /etc/locale.gen and uncomment en_US.UTF-8 UTF-8 and other needed locales.
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen

# Generate the locales 
locale-gen

# Edit locale.conf
echo "LANG=en_US.UTF-8" > /etc/locale.conf

# Set the console keyboard layout
echo "KEYMAP=us" > /etc/vconsole.conf

#------------------------------------------------------------------------------------
# Set hostname
echo "${hostName}" > /etc/hostname

# Create /etc/hots file
echo "127.0.0.1       localhost"  > /etc/hosts
echo "::1             localhost" >> /etc/hosts
echo "127.0.1.1       ${hostName}.localdomain ${hostName}" >> /etc/hosts

# recreate the initramfs image
mkinitcpio -P

# Set root password
echo root:${passWord} | chpasswd

# Install grub bootloader
pacman --noconfirm -S grub grub-btrfs efibootmgr os-prober
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=ArchGRUB
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=2/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

#====================================================================================#
#                                U S E R * S E T U P                                 #
#====================================================================================#
# Create user
useradd -m -G wheel -s /bin/bash ${userName}

# Create user password
 echo "${userName}:${passWord}" | chpasswd

# Install startship prompt
sh -c "$(curl -fsSL https://starship.rs/install.sh)"
echo 'eval "$(starship init bash)"'>> /home/${userName}/.bashrc

# Install arch packages
pacman --noconfirm --needed -S ${PKG}

# Install aur helper baph
aria2c https://bitbucket.org/archlabslinux/archlabs/raw/83d57b172bcce638da02d86a0f42ce30f77c3bdc/x86_64/archlabs-baph-1.5-1-x86_64.pkg.tar.zst
pacman --noconfirm -U archlabs-baph*
rm -rf archlabs-baph*

# Enable services to start at Boot
systemctl enable NetworkManager.service 

#====================================================================================#
#                                  T W E A C K S                                     #
#====================================================================================#
# Change sh to dash
rm /bin/sh && ln -sf /usr/bin/dash /bin/sh

# cleanup
exit
